# Cordova Plugin to Disable the iOS Status Bar

This plugin will hide the iOS status bar during the loading screen (and thereafter).
The regular `cordova-plugin-statusbar` does not provide that functionality, as it is
only active after `platform.isReady`. This plugin modifies the appropriate entries in
Info.plist to disable the status bar during launch. You can still use
`cordova-plugin-statusbar` to modify the state of the status bar afterwards.

## Using This Repository

To use this plugin in a Cordova-based application, add the plugin by
executing the following command-line:

```
cordova plugin add talisman-cordova-plugin-nostatusbarios
```
